# Phase10 Score Keeper

* [X] Add rounds
* [ ] Add ability to mark phase complete outside of finished round
* [ ] Store game logs
* [ ] Separate game summary into 2 pages: game status and finished summary
* [ ] Add unit tests
* [ ] Add timer for game with pause feature
* [ ] Add dealer marker
* [ ] Add ability to pick phases for the game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

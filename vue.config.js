module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/phase10-score-keeper/'
      : '/'
  }

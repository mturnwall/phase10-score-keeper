import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        numOfPhases: 10,
        players: []
    },
    mutations: {
        addPlayer(state, name) {
            state.players.push({
                id: state.players.length,
                name,
                currentPhase: 1,
                completedPhases: [],
                score: 0,
            });
        },
        updatePlayer(state, {index, player}) {
            state.players[index] = player;
        },
        removePlayer(state, index) {
            state.players.splice(index, 1);
        },
        updatePlayerScore(state, {index, value}) {
            state.players[index].score += value;
        },
        updatePlayerCurrentPhase(state, {id, value}) {
            state.players[id].currentPhase = value;
        },
    },
    actions: {
        addPlayer({commit}, {name}) {
            commit('addPlayer', name);
        },
        updatePlayers({commit}, {players}) {
            players.forEach((player, index) => {
                commit('updatePlayer', {
                    index,
                    player,
                });
            })
        },
        finishRound({commit}, {score, completedPhase}) {
            return new Promise((resolve) => {
                score.forEach((value, index) => {
                    commit('updatePlayerScore', {
                        value,
                        index
                    });
                });
                completedPhase.forEach((phaseObj) => {
                    commit('updatePlayerCurrentPhase', {
                        id: phaseObj.id,
                        value: phaseObj.phase,
                    });
                });
                resolve();
            });
        },
        removePlayer({commit}, {index}) {
            commit('removePlayer', index)
        }
    }
})

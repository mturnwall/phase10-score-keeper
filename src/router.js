import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import GameSummary from './views/GameSummary.vue';
import NewGame from './views/NewGame.vue';
import CompleteRound from './views/CompleteRound.vue';
import CurrentGame from './views/CurrentGame.vue';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/summary',
            name: 'summary',
            component: GameSummary
        },
        {
            path: '/new-game',
            name: 'new-game',
            component: NewGame,
        },
        {
            path: '/current-game',
            name: 'current-game',
            component: CurrentGame,
        },
        {
            path: '/complete-round',
            name: 'complete-round',
            component: CompleteRound,
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        }
    ]
});
